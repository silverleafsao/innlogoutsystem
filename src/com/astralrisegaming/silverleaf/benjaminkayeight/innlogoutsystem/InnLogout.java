//Author BenjaminKayEight and Navarr

package com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem;

import java.sql.SQLException;

import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.commands.CommandInn;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.listeners.InnLogoutListener;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.managers.PlayerNpcManager;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.objects.Inn;
import com.sharesc.caliog.npclib.NPCManager;
import com.silverleaf.util.BenjaminKayEightMysql;
import com.silverleaf.util.MessageLogger;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class InnLogout extends JavaPlugin{
	private static WorldGuardPlugin Wg = null;
	private static BenjaminKayEightMysql mysql = null;
	private static NPCManager npcman = null;
	private static FileConfiguration config = null;
	public static MessageLogger logger = null;

	//--------------------------------------OnDisable------------------------------
	public void onDisable(){
		this.getServer().getScheduler().cancelTasks(this);
		npcman.despawnAll();
		PlayerNpcManager.spawnednpcs.clear();
		try{
			mysql.getConnection().close();
		}catch(SQLException e){
			e.printStackTrace();
			logger.logConError("Problem closing mysql connection");
		}
		logger.logConMain("Disabled");
	}//end of OnDisable

	//--------------------------------------OnEnable-------------------------------
	public void onEnable(){

		logger = new MessageLogger(this);
		logger.logConMain("Enabled");
		//config setup
		config = this.getConfig();
		config.options().header("SilverleafSAO "+this.getName()+" Config");
		config.addDefault("Spawn Region Name", "spawn");
		config.addDefault("Database Name", "DBNAME");
		config.addDefault("Database Username", "DBUSERNAME");
		config.addDefault("Database Password", "DBPASSWORD");
		config.addDefault("NPC Logoout Time", 12000);
		config.options().copyDefaults(true);
		saveConfig();

		npcman = new NPCManager(this);
		new InnLogoutListener(this);

		Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
		if(plugin == null || !(plugin instanceof WorldGuardPlugin)){
			System.out.println("WorldGuard was not Loaded or was Missing!");
			Wg = null;
		}
		Wg = (WorldGuardPlugin) plugin;

		String database = config.getString("Database Name");
		String username = config.getString("Database Username");
		String password = config.getString("Database Password");
		mysql = new BenjaminKayEightMysql(database, username, password, this.getName(), logger);

		// Inn Singleton Initialization
		// Give the Inn class the mysql dependency
		Inn.loadMySQLDependency(mysql);
		// Tell Inn to create its table if its not there yet
		Inn.createTableIfNotExists();
		// Tell Inn to load all Inns from SQL into memory
		Inn.loadAllFromSQL();

		getCommand("inn").setExecutor(new CommandInn());

		//Lists all worlds on server
		logger.logConInfo("Worlds currently on this server:");
		for(World w : getServer().getWorlds()){
			logger.logConInfo(w.getName() + " ; ");
		}
	}//end of OnEnable

//-----------------------------------------EXTRAS-------------------------------------	

	public static WorldGuardPlugin getWorldGuard(){
		return Wg;
	}

	public static NPCManager getNpcManager(){
		return npcman;
	}

	/**
	 * Gets the Current Bukkit World the Player player is in as an (entity)
	 *
	 * @param
	 * @return World world
	 */
	public World getPlayerWorld(Player player){
		return player.getWorld();
	}

	/**
	 * Gets the WorldName as a string of the Player player passed.
	 *
	 * @param
	 * @return String worldname
	 */
	public String getPlayerWorldName(Player player){
		return player.getWorld().getName();
	}

	/**
	 * Returns the Bukkit World Entity from the name given
	 * if the world does not exist it will return NULL
	 *
	 * @param Wname
	 * @return World
	 */
	public World getWorldByName(String Wname){
		return getServer().getWorld(Wname);
	}
}//end of class