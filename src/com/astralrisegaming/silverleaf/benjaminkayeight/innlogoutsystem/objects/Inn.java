package com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.objects;

import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.InnLogout;
import com.silverleaf.util.*;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.protection.managers.RegionManager;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Inn Object
 * Inn's have a UNIQUE ID (Integer), a WORLD NAME, and a REGION NAME.
 * Also probably a cost.
 */
public class Inn{
	protected static Map<String, Map<String, Inn>> innsByWorld = new HashMap<>();
	protected static Map<Integer, Inn> innsById = new HashMap<>();

	protected static final String tableName = "innList";

	protected Integer id = null;
	private String regionBeforeSave = null;
	protected String region;
	private String worldBeforeSave = null;
	protected String world;

	protected static BenjaminKayEightMysql mysql;
	protected static MessageLogger logger;

	public Inn(String world, String region){
		this.setRegion(region);
		this.setWorld(world);
	}

	public static void loadMySQLDependency(BenjaminKayEightMysql mysql){
		Inn.mysql = mysql;
	}

	public static void loadLoggerDependency(MessageLogger logger){
		Inn.logger = logger;
	}

	public static void createTableIfNotExists(){
		String createInnTable = "CREATE TABLE IF NOT EXISTS " + Inn.tableName + " (id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, world VARCHAR(25) NOT NULL, region VARCHAR(25) NOT NULL, UNIQUE KEY world_region (world, region))";
		if(!(Inn.mysql.createTable(createInnTable))){
			logger.logConError("Inn could not create table innList");
		}
	}

	public static void loadAllFromSQL(){
		ResultSet innList = Inn.mysql.resGet("SELECT id,world,region FROM " + Inn.tableName);
		try{
			while(innList != null && innList.next()){
				Inn.loadInnFromResultSet(innList);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public static Inn loadInnFromResultSet(ResultSet rs){
		try{
			Integer id = rs.getInt("id");
			String world = rs.getString("world");
			String region = rs.getString("region");

			Inn inn = new Inn(world, region);
			inn.id = id;
			inn.loadIntoMaps();
			return inn;
		}
		catch(SQLException e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param name
	 * @return Inn|null Returns the Inn if found, or null if not.
	 */
	public Inn find(String world, String name){
		world = world.toLowerCase();
		name = name.toLowerCase();

		if(!Inn.innsByWorld.containsKey(world) || !Inn.innsByWorld.get(world).containsKey(name)){
			return null;
		}
		return Inn.innsByWorld.get(world).get(name);
	}

	/**
	 * @param worldName
	 * @return A List of all Inns in the given world.
	 */
	public static List<Inn> findAllByWorld(String worldName){
		worldName = worldName.toLowerCase();

		List<Inn> innList = new ArrayList<>();
		if(Inn.innsByWorld.containsKey(worldName)){
			for(Map.Entry<String, Inn> entry : Inn.innsByWorld.get(worldName).entrySet()){
				innList.add(entry.getValue());
			}
		}
		return innList;
	}

	public static boolean exists(String world, String region){
		world = world.toLowerCase();
		region = region.toLowerCase();

		return Inn.innsByWorld.containsKey(world) && Inn.innsByWorld.get(world).containsKey(region);
	}

	public void save() throws Exception{
		String sql;
		if(this.id == null){
			sql = "INSERT INTO " + tableName + "(world,region) VALUES (?, ?)";
			PreparedStatement query = Inn.mysql.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			query.setString(1, this.getWorld());
			query.setString(2, this.getRegion());
			query.execute();
			ResultSet keys = query.getGeneratedKeys();
			keys.next();
			this.id = keys.getInt(1);
			this.loadIntoMaps();
		}
		else{
			sql = "UPDATE " + tableName + " SET world=?, region=? WHERE id=?";
			PreparedStatement query = Inn.mysql.getConnection().prepareStatement(sql);
			query.setString(1, this.getWorld());
			query.setString(2, this.getWorld());
			query.setInt(3, this.id);
			query.execute();
			if(this.worldBeforeSave != null || this.regionBeforeSave != null){
				String oldWorld = this.worldBeforeSave != null ? this.worldBeforeSave : this.world;
				String oldRegion = this.regionBeforeSave != null ? this.regionBeforeSave : this.region;
				Inn.innsByWorld.get(oldWorld).remove(oldRegion);
				this.loadIntoMaps();
			}
		}
	}

	private void loadIntoMaps(){
		if(this.id == null || this.world == null || this.region == null){
			logger.logConInfo("Attempted to load an Inn into the maps before data is set");
		}
		else{
			Inn.innsById.put(this.id, this);
			if(!Inn.innsByWorld.containsKey(this.getWorld())){
				Inn.innsByWorld.put(this.getWorld(), new HashMap<String, Inn>());
			}
			Inn.innsByWorld.get(this.getWorld()).put(this.getRegion(), this);
		}
	}

	/**
	 * Deletes all Inns with a specific region
	 *
	 * @param world
	 * @param region
	 * @return
	 */
	public static int delete(String world, String region){
		world = world.toLowerCase();
		region = region.toLowerCase();

		String sql = "DELETE FROM " + tableName + " WHERE world=? AND region=? LIMIT 1";
		int count;
		try{
			PreparedStatement query = Inn.mysql.getConnection().prepareStatement(sql);
			query.setString(1, world);
			query.setString(2, region);
			query.execute();
			count = query.getUpdateCount();
		}
		catch(SQLException e){
			e.printStackTrace();
			count = -1;
		}
		if (Inn.innsByWorld.containsKey(world) && Inn.innsByWorld.get(world).containsKey(region)) {
			Inn inn = Inn.innsByWorld.get(world).get(region);
			Inn.innsById.remove(inn.id);
			Inn.innsByWorld.get(world).remove(region);
		}

		return count;
	}

	public boolean delete(){
		return Inn.delete(this.getWorld(), this.getRegion()) > 0;
	}

	/* Getters and Setters */

	public String getRegion(){
		return this.region;
	}

	// This is private because you can not rename Inns.
	private void setRegion(String region){
		if(this.id != null && this.regionBeforeSave == null && !region.equalsIgnoreCase(this.region)){
			this.regionBeforeSave = this.region;
		}
		this.region = region.toLowerCase();
	}

	public String getWorld(){
		return this.world;
	}

	// This is private because you can not move an Inn between worlds.
	private void setWorld(String world){
		if(this.id != null && this.worldBeforeSave == null && !world.equalsIgnoreCase(this.world)){
			this.worldBeforeSave = this.world;
		}
		this.world = world.toLowerCase();
	}

	/**
	 * Checks if the player is stood within an inn
	 *
	 * @param player
	 * @return
	 */
	public static String isInInn(Player player){

		final World world = player.getWorld();
		final Location loc = player.getLocation();
		final Vector vec = new Vector(loc.getX(), loc.getY(), loc.getZ());
		RegionManager mgr = InnLogout.getWorldGuard().getRegionManager(world);

		List<String> locationRegions = mgr.getApplicableRegionsIDs(vec);

		Map<String, Inn> innsForWorld = Inn.innsByWorld.get(world.getName());

		for(String region : locationRegions){
			if(innsForWorld.containsKey(region)){
				return region;
			}
		}

		return null;
	}

	//TODO add cost checking when players have to pay for an inn
	public void hasPaid(Player player){

	}

}
