package com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.commands;

import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.InnLogout;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.objects.Inn;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

//TODO whenever an inn is added convert it to lower case so .contains works
//TODO readd cost later
public class CommandInn implements CommandExecutor{

	//add an inn remove an inn
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(sender instanceof Player){
			Player player = (Player) sender;
			String name = player.getName();
			String worldName = player.getWorld().getName();
			World world = player.getWorld();

			if(args.length == 0){
				InnLogout.logger.logPlayerInfo(player, "inn add [region name]");
				InnLogout.logger.logPlayerInfo(player, "inn remove [inn name]");
				return true;
			}
			else if(args.length == 1){
				if(args[0].equalsIgnoreCase("add")){
					InnLogout.logger.logPlayerInfo(player, "inn add [region name] ");
				}
				else if(args[0].equalsIgnoreCase("remove")){
					InnLogout.logger.logPlayerInfo(player, "Possible inns to remove in this world:");

					//list of every inn on the players current world
					List<Inn> inns = Inn.findAllByWorld(worldName);
					//TODO show all inns on one line :D
					for(Inn inn : inns){
						InnLogout.logger.logPlayerPass(player, inn.getRegion());
					}
					return true;
				}
				InnLogout.logger.logPlayerWarning(player, "Invalid command");
				return true;
			}
			else if(args.length == 2){
				if(args[0].equalsIgnoreCase("add")){
					if(Inn.exists(worldName, args[1])){
						InnLogout.logger.logConWarning("That Inn already Exists");
						return true;
					}
					else{
						// Check to ensure the region exists
						WorldGuardPlugin wG = InnLogout.getWorldGuard();
						RegionManager rMan = wG.getRegionManager(world);
						if(!rMan.hasRegion(args[1])){
							InnLogout.logger.logPlayerInfo(player, "That region does not exist on this floor");
							return true;
						}

						Inn inn = new Inn(worldName, args[1]);
						try{
							InnLogout.logger.logPlayerInfo(player, "Created Inn:" + args[1].toLowerCase());
							InnLogout.logger.logConInfo("Player: " + name + " Created Inn: " + args[1].toLowerCase() + " : in world: " + world);
							inn.save();
						}
						catch(Exception e){
							e.printStackTrace();
							InnLogout.logger.logPlayerInfo(player, "Failed Creating Inn: " + args[1].toLowerCase() + " (Error: Already exists)");
							InnLogout.logger.logConInfo("Player: " + name + " Failed to Create Inn: " + args[1].toLowerCase() + " in world: " + world + " because it already existed");
						}
						return true;
					}
				}
				else if(args[0].equalsIgnoreCase("remove")){

					if(Inn.exists(worldName, args[1])){
						Inn.delete(worldName, args[1]);
						InnLogout.logger.logPlayerInfo(player, "Removed Inn:" + args[1].toLowerCase());
						InnLogout.logger.logConWarning("Player: " + name + " Removed Inn: " + args[1].toLowerCase() + " : in world: " + worldName);
						return true;
					}
				}
			}
			else if(args.length == 3){//adding an inn with a cost
				if(args[0].equalsIgnoreCase("add")){
					// TODO Add cost stuff to Inn class
					InnLogout.logger.logPlayerWarning(player, "Not yet implemented");
					return true;
				}
				InnLogout.logger.logPlayerWarning(player, "Invalid command");
				return true;
			}
		}//end of if player
		//TODO console ?
		return false;
	}//end of on command
}//end of class
