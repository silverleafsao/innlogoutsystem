package com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.listeners;

import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.InnLogout;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.managers.PlayerNpcManager;
import com.astralrisegaming.silverleaf.benjaminkayeight.innlogoutsystem.objects.Inn;
import com.sharesc.caliog.npclib.NPCManager;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;

//TODO on login
//TODO use npcid instead of player (hashmap name id) id for name remove
public class InnLogoutListener implements Listener{

	private NPCManager npcman = null;
	private InnLogout plugin = null;
	private FileConfiguration config;
	private Map<String, Integer> runnableMap = new HashMap<>();
	// 10 mins = 12000
	private long autoLogoutDespawnTime;

	public InnLogoutListener(InnLogout plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
		npcman = InnLogout.getNpcManager();
		config = plugin.getConfig();
		autoLogoutDespawnTime = config.getLong("NPC Logoout Time");
	}

	@EventHandler
	public void onPlayerLogout(PlayerQuitEvent event){
		final Player player = event.getPlayer();
		final String name = player.getName();
		final Location loc = player.getLocation();
		final String worldName = player.getWorld().getName();
		// change this section my making getnpcmanager return false ?
		if(npcman == null){
			npcman = InnLogout.getNpcManager();
		}
		if(npcman == null){
			InnLogout.logger.logConWarning("NPC Manager is NULL in PlayerLogoutEvent");
		}
		else{
			if(PlayerNpcManager.spawnednpcs.contains(name)){
				npcman.despawnHumanByName(name);
				PlayerNpcManager.spawnednpcs.remove(name);
			}

			if(Inn.isInInn(player) != null){
				//TODO quit message to be removed post alpha
				event.setQuitMessage("Player: " + name + " sucessfully logged out in an Inn.");
				if(PlayerNpcManager.spawnednpcs.contains(name)){
					npcman.despawnHumanByName(name);
					PlayerNpcManager.spawnednpcs.remove(name);
				}
				InnLogout.logger.logConInfo("Player: " + name + " Succesfully logged out from an Inn");
			}
			else{//not in an inn (is inn returned null)
				if(!PlayerNpcManager.spawnednpcs.contains(name))
					PlayerNpcManager.spawnednpcs.add(name);
				npcman.spawnHumanNPC(name, loc);
				InnLogout.logger.logConWarning("Player: " + name + " Logged out while not in an Inn in level: " + worldName + " At location: x:" + loc.getX() + " z:" + loc.getZ() + " y:" + loc.getY());
				InnLogout.logger.logConInfo("Logout Npc: " + name + " was created");

				//set up auto despawn
				if(plugin != null){
					Runnable toRun = new Runnable(){
						public void run(){
							if(PlayerNpcManager.spawnednpcs.contains(name)){
								npcman.despawnHumanByName(name);
								PlayerNpcManager.spawnednpcs.remove(name);

								InnLogout.logger.logConInfo("Auto Despawn of Logout NPC: " + name);
							}
							else{
								InnLogout.logger.logConInfo("Auto Despawn of Logout NPC: " + name + "Did not occure due to name not being present on list");
							}
						}
					};
					int taskId = plugin.getServer().getScheduler().runTaskLater(plugin, toRun, autoLogoutDespawnTime).getTaskId();
					this.runnableMap.put(name, taskId);
				}
			}
		}
	}//end of on player quit event

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerJoin(PlayerJoinEvent event){
		final Player player = event.getPlayer();
		final String name = player.getName();

		if(PlayerNpcManager.spawnednpcs.contains(name)){

			try{
				InnLogout.logger.logPlayerInfo(player, "You recently (in the past hour) did not log out within an InnRoom");
				InnLogout.logger.logPlayerInfo(player, "Because of this an NPC was spawned in your location. Please be careful next time");
				npcman.despawnHumanByName(name);
				PlayerNpcManager.spawnednpcs.remove(name);
				if(this.runnableMap.containsKey(name)){
					plugin.getServer().getScheduler().cancelTask(this.runnableMap.get(name));
					this.runnableMap.remove(name);
				}
				InnLogout.logger.logConInfo("Despawn of NPC: " + name + " Was successful");
			}
			catch(Exception e){
				InnLogout.logger.logConError("InnLogout OnPlayerJoin SL2ILS008");
				e.printStackTrace();
			}
		}
	}

	//TODO death consequences - change to death event not damage event just set npc to have 1 hp
	@EventHandler(priority = EventPriority.HIGH)
	public void onNPCDeath(PlayerDeathEvent event){
		final Player player = (Player) event.getEntity();
		final String name = player.getName();

		if(npcman.isNPC(event.getEntity())){
			//TODO DEATH STUFF
			if(PlayerNpcManager.spawnednpcs.contains(name)){
				npcman.despawnHumanByName(name);
				PlayerNpcManager.spawnednpcs.remove(name);
				InnLogout.logger.logConInfo("Logout Npc was killed: " + name);
			}
		}

		// when an entity is damaged it checks to see if they are an npc
		// then if it is it adds the death conditions
		/* player goes to spawn point for closest city or home location
		 * (add a log of spawn locations for players if applicable)
		 * player also loses stats and basic items and money
		 * for npc kill enemy player may or may not get items and money
		 * add to killer list ??? maybe maybe not
		 * */
	}

}// end of class
